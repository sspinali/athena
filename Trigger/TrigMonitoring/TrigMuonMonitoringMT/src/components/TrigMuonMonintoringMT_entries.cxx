/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "../L1MuonMonMT.h"
#include "../L2MuonSAMonMT.h"
#include "../L2MuonSAIOMonMT.h"
#include "../L2muCombMonMT.h"
#include "../L2OverlapRemoverMonMT.h"
#include "../EFMuonMonMT.h"
#include "../TrigMuonEfficiencyMonMT.h"
#include "../MuonMatchingTool.h"
#include "../MuonTriggerCountMT.h"

DECLARE_COMPONENT( L1MuonMonMT )
DECLARE_COMPONENT( L2MuonSAMonMT )
DECLARE_COMPONENT( L2MuonSAIOMonMT )
DECLARE_COMPONENT( L2muCombMonMT )
DECLARE_COMPONENT( L2OverlapRemoverMonMT )
DECLARE_COMPONENT( EFMuonMonMT )
DECLARE_COMPONENT( TrigMuonEfficiencyMonMT )
DECLARE_COMPONENT( MuonMatchingTool )
DECLARE_COMPONENT( MuonTriggerCountMT )

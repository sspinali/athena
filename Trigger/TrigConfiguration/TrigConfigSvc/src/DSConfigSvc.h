/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TrigConfig_DSConfigSvc
#define TrigConfig_DSConfigSvc

#include "ConfigSvcBase.h"
#include "TrigConfInterfaces/ITrigConfigSvc.h"

#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/extends.h"
#include "AthenaKernel/IOVSvcDefs.h"
#include "CxxUtils/checker_macros.h"
#include "StoreGate/StoreGateSvc.h"

#include "TrigConfL1Data/CTPConfig.h"
#include "TrigConfHLTData/HLTFrame.h"
#include "L1TopoConfig/L1TopoMenu.h"
#include "TrigConfData/L1Menu.h"
#include "TrigConfData/L1PrescalesSet.h"
#include "TrigConfData/L1BunchGroupSet.h"
#include "TrigConfData/HLTMenu.h"
#include "TrigConfData/HLTPrescalesSet.h"

#include <stdint.h>
#include <memory>
#include <set>
#include <string>
#include <vector>

class EventContext;

/**
 *    @short Service providing the full trigger configuration information, based
 *           on what is provided by HLTConfigSvc and LVL1ConfigSvc
 */
namespace TrigConf {

   class ThresholdConfig;
   class HLTChainList;
   class HLTSequenceList;
   class BunchGroupSet;
   class Muctpi;

   /**
    * @brief Service providing the full trigger configuration information, based
    *        on what is provided by HLTConfigSvc and LVL1ConfigSvc
    *
    */
   class DSConfigSvc : public extends<ConfigSvcBase, ITrigConfigSvc>
   {

   public:

      // Standard Gaudi Service constructor
      DSConfigSvc( const std::string& name, ISvcLocator* pSvcLocator );

      // @brief initialize the service
      virtual StatusCode initialize ATLAS_NOT_THREAD_SAFE () override;

      // LVL1 menu and prescales only (no bunchgroups, prescale clocks, etc.)
      virtual const CTPConfig* ctpConfig() const override {
         return & m_ctpConfig;
      }

      // access muctpi configuration
      virtual const Muctpi* muctpiConfig() const override {
         return & m_ctpConfig.muCTPi();
      }

      // access to trigger thresholds
      virtual const ThresholdConfig* thresholdConfig() const override {
         return & m_ctpConfig.menu().thresholdConfig();
      }

      // returns bunch group set
      virtual const BunchGroupSet* bunchGroupSet() const override {
         return & m_ctpConfig.bunchGroupSet();
      }

      // access to HLT chains
      virtual const HLTChainList& chains() const override {
         return m_hltFrame.getHLTChainList();
      }

      // access to HLT sequences
      virtual const HLTSequenceList& sequences() const override {
         return m_hltFrame.getHLTSequenceList();
      }

      // access to SMK
      virtual uint32_t masterKey() const override { return m_masterKey; }

      // access to LVL1 prescale key
      virtual uint32_t lvl1PrescaleKey() const override {
         return m_lvl1PsKey;
      }

      // access to HLT prescale configuration key
      virtual uint32_t hltPrescaleKey() const override { return m_hltPsKey; }

      // access to a description of the source of the configuration (TriggerDB/XMLfilename)
      virtual std::string configurationSource() const override {
         return m_configSrc;
      }

      /// @name Dummy implementations of the Run 3 L1 JSON trigger configuration interface in IILVL1ConfigSvc.
      /// @brief Use the xAODConfigSvc or xAODConfigTool to access these data.
      /// @{
      virtual const ::TrigConf::L1Menu& l1Menu(const ::EventContext&) const override {
        const static ::TrigConf::L1Menu dummy = ::TrigConf::L1Menu();
        return dummy;
      }

      virtual const ::TrigConf::L1PrescalesSet& l1PrescalesSet(const ::EventContext&) const override {
        const static ::TrigConf::L1PrescalesSet dummy = ::TrigConf::L1PrescalesSet();
        return dummy;
      }

      virtual const ::TrigConf::L1BunchGroupSet& l1BunchGroupSet(const ::EventContext&) const override {
        const static ::TrigConf::L1BunchGroupSet dummy = ::TrigConf::L1BunchGroupSet();
        return dummy;
      }
      /// @}

      /// @name Dummy implementations of the Run 3 HLT JSON trigger configuration interface in IIHLTConfigSvc.
      /// @brief Use the xAODConfigSvc or xAODConfigTool to access these data.
      /// @{
      virtual const ::TrigConf::HLTMenu& hltMenu(const ::EventContext&) const override {
         const static ::TrigConf::HLTMenu dummy = ::TrigConf::HLTMenu();
         return dummy;
      }

      virtual const ::TrigConf::HLTPrescalesSet& hltPrescalesSet(const ::EventContext&) const override {
         const static ::TrigConf::HLTPrescalesSet dummy = ::TrigConf::HLTPrescalesSet();
         return dummy;
      }
      /// @}

   private:
      StatusCode registerCallbackForFolder ATLAS_NOT_THREAD_SAFE ( const std::string& foldername,
                                                                   bool multichannel );
      StatusCode update ATLAS_NOT_THREAD_SAFE ( IOVSVC_CALLBACK_ARGS_P( I, keys ) );
      StatusCode reset();
      void setEFLowerChainCounter();
      void set_ChainlistFromHltPrescaleSet();
      void set_HltPrescaleSetFromChainlist();

      StatusCode loadMenuFromDB( uint32_t smk, uint32_t l1psk, uint32_t hltpsk, uint32_t bgsk );

      std::unique_ptr<TXC::L1TopoMenu> m_topoMenu;

      CTPConfig            m_ctpConfig;       ///< LVL1 configuration
      HLTFrame             m_hltFrame;        ///< HLT configuration

      uint32_t             m_masterKey;       ///< configuration master key
      uint32_t             m_lvl1PsKey;       ///< lvl1 prescale key
      uint32_t             m_hltPsKey;        ///< hlt prescale key
      uint32_t             m_lvl1BgKey;       ///< lvl1 bunchgroup key
      std::string          m_configSrc;       ///< Description of the source of the configuration


      /// Handle to the Detector Store
      ServiceHandle< StoreGateSvc > m_detstore;

      ///to avoid crashes when a folder is not present and allow for lvl1/htl separation : 
      /// /* m_folders will take the info on folders from IOVDbSvc*/
      std::set<std::string> m_folders;


      // hasFolder(folder_name) will return true if folder_name is found in the string returned by IOVDbSvc, false otherwise
      bool hasFolder( const std::string& folder_name );

   }; // class DSConfigSvc

} // namespace TrigConf

#endif // TrigConfig_DSConfigSvc__H
